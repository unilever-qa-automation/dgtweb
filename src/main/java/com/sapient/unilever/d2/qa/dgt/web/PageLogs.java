/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sapient.unilever.d2.qa.dgt.web;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.sapient.unilever.d2.qa.dgt.web.db.DBManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Sachin
 */
public class PageLogs extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String address = request.getParameter("url");
            String report = request.getParameter("report");
            String browser = request.getParameter("browser");
            String jsonp = request.getParameter("jsonp");
            BasicDBObject query = new BasicDBObject("url", address);
            query.append("browser", browser);
            JSONObject json = new JSONObject();
            JSONArray arr = new JSONArray();

            try (DBManager mngr = new DBManager()) {
                MongoDatabase db = mngr.getMongoDB();
                FindIterable<Document> find = db.getCollection(report).find(query);
                for (Document url : find) {
                    JSONObject urlData=new JSONObject();
                    urlData.put("url", url.getString("url"));
                    urlData.put("browser", url.getString("browser"));
                    urlData.put("type", url.getString("type"));  
                    urlData.put("logs", new JSONObject(url.getString("errors")));
                    arr.put(urlData);
                }
                json.put("rows", arr);
                json.put("total_rows", arr.length());
            } catch (Exception ex) {
                out.print("error in fetching results from DB. " + ex);
            }
            out.print(jsonp + "(" + json + ")");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
