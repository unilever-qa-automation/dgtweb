/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sapient.unilever.d2.qa.dgt.web;

import com.sapient.unilever.d2.qa.dgt.web.build.JenkinsBuilder;
import com.sapient.unilever.d2.qa.dgt.web.utils.HelperUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

/**
 *
 * @author sku202
 */
public class Start extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String buildType = request.getParameter("buildType");
            String crawlerConfigFile = request.getParameter("CrawlerConfigFile");
            String urlsTextFile = request.getParameter("urlsTextFile");
            String email = request.getParameter("email");
            String brandName = request.getParameter("brandName");
            String[] browsers = request.getParameterValues("browsers");
            String imageDiff = request.getParameter("imageDiff").equalsIgnoreCase("on") ? "Yes" : "No";
            String preBuildTime = request.getParameter("preBuildTime");
            String siteURL = request.getParameter("siteURL");
            String jsDiff = request.getParameter("jsDiff").equalsIgnoreCase("on") ? "Yes" : "No";
            String preBranchVersion = request.getParameter("preBranchVersion");
            String username = "", password = "";
            if (buildType.equalsIgnoreCase("pre")) {
                jsDiff = "No";
                imageDiff = "No";
            } else if (buildType.equalsIgnoreCase("post")) {
                brandName = request.getParameter("brandNames");
            }
            if (!StringUtils.isEmpty(request.getParameter("username"))) {
                username = request.getParameter("username");
                password = request.getParameter("password");
            }
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            Map<String, String> map = new HashMap<>();
            map.put("BrandName", brandName);
            map.put("SiteAddress", siteURL);
            map.put("Username", username);
            map.put("Password", password);
            map.put("UrlsTextFileLocation", urlsTextFile);
            map.put("imageDiff", imageDiff);
            map.put("jsDiff", jsDiff);
            map.put("BuildType", buildType);
            map.put("PreBuildVersion", preBranchVersion);
            map.put("PreBuildTime", preBuildTime);
            map.put("CrawlerConfigFile", crawlerConfigFile);
            map.put("BrowserConfigFile", getBrowserConfig(browsers));
            map.put("email", email);
            map.put("machine", ipAddress);

            String jenkinsUrl = getServletContext().getInitParameter("jenkinsUrl");
            String jenkinsJob = getServletContext().getInitParameter("jenkinsJob");
            JenkinsBuilder builder = new JenkinsBuilder(jenkinsUrl, jenkinsJob);
            out.print(builder.build(map));

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getBrowserConfig(String[] browsers) {
        try {
            String dir = getServletContext().getRealPath("assets") + File.separator + "browsers.json";
            File file = new File(getServletContext().getRealPath("upload") + File.separator + "browsers" + File.separator
                    + HelperUtils.generateUniqueString());
            file.mkdirs();
            JSONObject json = new JSONObject(FileUtils.readFileToString(new File(dir), "utf-8"));
            for (String name : browsers) {
                json.getJSONObject("broswers").getJSONObject(name).put("enable", true);
            }
            File f = new File(file, "browsers.json");
            Writer writer = new FileWriter(f);
            json.write(writer);
            writer.close();
            return f.getAbsolutePath();
        } catch (IOException ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

}
