<%-- 
    Document   : index
    Created on : Jan 9, 2017, 12:42:29 PM
    Author     : Sachin Kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String address = request.getRequestURL().toString().substring(0, request.getRequestURL().toString().lastIndexOf('/'));
    session.setAttribute("address", address);

%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/jspf/head.jspf"%>
    <title>DGT</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link href="${pageContext.request.contextPath}/assets/css/main.css" rel="stylesheet"> 
    <body role="document">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 sidebar" id="sidebar">
                    <%@include file="/WEB-INF/jspf/sidebar.jspf"%>
                </div>
                <div class="col-md-10" id="main1">
                    <div class="" id="main2">
                        <div class="row">
                            <div class="" id="sachin">
                                <div class="tool-header">
                                    <h1>Get Started...</h1>
                                    <h4>Difference Generation Tool</h4>
                                </div>
                            </div>
                            <div class="messages"></div>
                            <div class="hidden" id="mask">
                                <div class="col-md-6 col-md-offset-3 mask-data">
                                    <img src="${pageContext.request.contextPath}/assets/img/spin.gif" class="img-responsive center-block"/>     
                                    <h3>Processing...</h3>                                    
                                    <div id="info">
                                        <div id="status"></div>
                                        <div id="msgs"><p></p></div>
                                        <input type="hidden" id="buildID" name="buildID" value="">
                                        <input type="hidden" id="buildUrl" name="buildUrl" value="">                                        
                                        <input type="hidden" id="queueUrl" name="queueUrl" value="">
                                        <input type="hidden" id="type" name="type" value="">
                                        <input type="hidden" id="start" name="start" value="0">
                                    </div>
                                    <div class="hidden" id='bar-mask'>                                        
                                        <div class="progress" >
                                            <div id="orangeBar-mask" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                                        </div>  
                                    </div>
                                </div>
                                <div class='clearfix'></div>
                            </div>
                            <div id="content">
                                <form id="single-site-form" name="single-site-form" action="Start" method="POST" enctype="multipart/form-data"> 
                                    <div id='page' name='page'>
                                        <div class="section">Site Info: </div>
                                        <div class="section-data"> 
                                            <div class="row buildType" >
                                                <label for="buildType" class="col-md-2 col-form-label">Build Type: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group">   
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-cloud" aria-hidden="true"></span>
                                                            </span>                                        
                                                            <select id="buildType" name="buildType" class="form-control selectpicker show-menu-arrow show-tick" title ="Select Build Type" data-toggle="tooltip">
                                                                <option value="Pre">Pre</option>
                                                                <option value="Post">Post</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row siteURL" >
                                                <label for="siteURL" class="col-md-2 col-form-label">Site Address: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group">   
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                                                            </span>                                        
                                                            <input type="text" class="form-control" placeholder="http://example.com" id="siteURL" name="siteURL" title ="Enter URL with http:// or https://" data-toggle="tooltip">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row brandName" >
                                                <label for="brandName" class="col-md-2 col-form-label">Brand Name: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group" id="brandNameInput">   
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span>
                                                            </span>                                        
                                                            <input type="text" class="form-control" placeholder="Brand Name" id="brandName" name="brandName" title ="Enter brand Name" data-toggle="tooltip">                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group hidden" id="brandNameSelect">   
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span>
                                                            </span>  
                                                            <select id="brandNames" name="brandNames" class="form-control selectpicker show-menu-arrow show-tick" title ="Select Brand Name" data-toggle="tooltip">
                                                                <option selected>Choose Brand Name</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="URLfile row">
                                                <label for="URLfile" class="col-md-2 col-form-label">Urls From File: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group ">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-import" aria-hidden="true"></span>
                                                            </span>                                        
                                                            <input type="file" class="form-control " placeholder="example.txt" id="URLfile" name="URLfile" title ="Browse text file containing urls" data-toggle="tooltip">
                                                        </div>
                                                        <div class=''>
                                                            <div class="hidden" id='bar'>
                                                                <div class="progress" >
                                                                    <div id="orangeBar" class="progress-bar progress-bar-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                                                                </div>  
                                                            </div> 
                                                            <div class='clearfix'></div>
                                                        </div>                                                       
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row authentication">
                                                <div class="col-md-2" >
                                                    <input type="checkbox" name="setAuthentication" id="setAuthentication">
                                                    <label for="setAuthentication" class="col-form-label"> Authentication</label>
                                                </div> 
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="input-group auth">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                                                </span>                                        
                                                                <input type="text" class="form-control" disabled placeholder="Username" id="username" name="username" title ="Enter LDAP username for site" data-toggle="tooltip">
                                                            </div> 
                                                        </div>
                                                        <div class="col-md-6 "> 
                                                            <div class="input-group  auth">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                                </span>                                        
                                                                <input type="password" class="form-control" disabled placeholder="Password" id="password" name="password" title ="Enter LDAP password for site" data-toggle="tooltip">
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>                                          
                                        </div>
                                    </div>
                                    <div class='clearfix'></div>
                                    <div id="suite" class="">
                                        <div class='section'>Suite Info: </div>
                                        <div class="section-data">                                             
                                            <div class="browsers row">                 
                                                <label for="browsers" class="col-md-2 col-form-label">Browsers: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group "> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                                                            </span>
                                                            <select id="browsers" name="browsers" class="form-control selectpicker show-menu-arrow show-tick" title ="Select Browsers" data-toggle="tooltip" multiple>
                                                                <option value="chrome">Google Chrome</option>
                                                                <option value="firefox">Mozilla Firefox</option>
                                                                <option value="ie">Internet Explorer</option>
                                                                <option value="phantom">Phantom JS</option>
                                                                <option value="edge">Microsoft Edge</option>
                                                                <option value="iphone_with_chrome_emulation">iPhone using Chrome Emulation</option>
                                                                <option value="andriod_with_chrome_emulation">Android using Chrome Emulation</option>
                                                                <option value="tablet_with_chrome_emulation">Tablet using Chrome Emulation</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preBranchVersion row hidden">                 
                                                <label for="preBranchVersion" class="col-md-2 col-form-label">Pre Branch Version: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group "> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                            </span>
                                                            <select id="preBranchVersion" name="preBranchVersion" class=" form-control selectpicker show-menu-arrow show-tick" title ="Select Pre Branch Version to compare with" data-toggle="tooltip">
                                                                <option selected>Choose Pre Branch Version</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preBuildTime row hidden">                 
                                                <label for="preBuildTime" class="col-md-2 col-form-label">Pre Build Time: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group "> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                            </span>
                                                            <select id="preBuildTime" name="preBuildTime" class="form-control  selectpicker show-menu-arrow show-tick" title ="Select Pre Build Time to compare with" data-toggle="tooltip" >
                                                                <option selected>Choose Pre Build Time</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="diff row hidden">
                                                <label class="col-md-2 col-form-label">Difference Type: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group row">
                                                        <div class="col-md-6 " title ="Select to choose for image bases comparison" data-toggle="tooltip" >
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
                                                                </span> 
                                                                <input type="text" class="form-control " aria-label="..." disabled value="Image Difference">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox" class="" id="imageDiff" aria-label="..." name="imageDiff" checked>
                                                                </span> 
                                                            </div>
                                                        </div>     
                                                        <div class=" col-md-6 " title ="Select to choose for browser console error" data-toggle="tooltip">
                                                            <div class="input-group">                                                               
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                                                                </span> 
                                                                <input type="text" class="form-control " aria-label="..." disabled  value="Browser Console Errors">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox" class="" id="jsDiff" aria-label="..." name="jsDiff" checked>
                                                                </span>     

                                                            </div>
                                                        </div> 
                                                        <!--
                                                        <div class="col-md-4 ">
                                                            <div class="input-group" >
                                                                <input type="text" class="form-control " aria-label="..." disabled title ="Select to choose for HTML differences" data-toggle="tooltip" value="HTML Differences">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox" class="" id="htmlDiff" aria-label="..." name="htmlDiff" title ="Select to choose for HTML differences" data-toggle="tooltip" disabled>
                                                                </span>                                                            
                                                            </div>
                                                        </div> 
                                                        -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="crawler row">
                                                <label for="fileURL" class="col-md-2 col-form-label">Crawler Config: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group ">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                                            </span>                                        
                                                            <input type="file" class="form-control " placeholder="Config.properties" id="fileURL" name="fileURL" title ="Browse Configuration file for crawler. Config.properties file" data-toggle="tooltip">
                                                        </div>
                                                        <div class=''>
                                                            <div class="hidden" id='bar'>
                                                                <div class="progress" >
                                                                    <div id="orangeBar" class="progress-bar progress-bar-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                                </div>  
                                                            </div> 
                                                            <div class='clearfix'></div>
                                                        </div> 
                                                        <div class=''>
                                                            <p>View Sample File: <a target="_blank" href="http://10.207.16.9/DGT/Config.properties"> http://10.207.16.9/DGT/Config.properties</a> </p>
                                                            <div class='clearfix'></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row emails" >
                                                <label for="emails" class="col-md-2 col-form-label">Email: </label>
                                                <div class="col-md-10  form-section row">
                                                    <div class="form-group ">
                                                        <div class="input-group ">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                                            </span>                                        
                                                            <input type="text" class="form-control" placeholder="abc@sapient.com" id="email" name="email" title ="Enter Emails seperated by commas" data-toggle="tooltip">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" >                                               
                                                <div class="col-md-offset-2 col-md-10  form-section">
                                                    <div class="form-group ">
                                                        <button class="btn btn-primary pull-right col-md-2 sbmt" type="submit">Validate</button>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class='clearfix'></div>
                                    <div id="results" class="">
                                        <div class='section'>Results: </div>
                                        <div class="section-data"> 
                                            <div id='resultsData' class="">
                                                <div id="data"></div>
                                                <div id="fountainG" class="result-image hidden">
                                                    <div id="fountainG_1" class="fountainG"></div>
                                                    <div id="fountainG_2" class="fountainG"></div>
                                                    <div id="fountainG_3" class="fountainG"></div>
                                                    <div id="fountainG_4" class="fountainG"></div>
                                                    <div id="fountainG_5" class="fountainG"></div>
                                                    <div id="fountainG_6" class="fountainG"></div>
                                                    <div id="fountainG_7" class="fountainG"></div>
                                                    <div id="fountainG_8" class="fountainG"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class='clearfix'></div>  
                                    <div class="hidden">
                                        <input type="hidden" name="CrawlerConfigFile" id="CrawlerConfigFile" value="">
                                        <input type="hidden" name="urlsTextFile" id="urlsTextFile" value="">
                                    </div>
                                </form>
                            </div>
                            <%@include file="/WEB-INF/jspf/footer.jspf"%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footerScripts.jspf"%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/main.js"></script>  
    </body>
</html>
