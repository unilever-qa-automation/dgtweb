$(document).ready(function () {
    $('#mask').css('min-height', $('#single-site-form>div').height() + 330);
    var formClone = $("#single-site-form").clone();
    $('#single-site-form')
            .find('[name="buildType"]')
            .selectpicker()
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'buildType');
            }).end()
            .find('select#brandNames')
            .selectpicker()
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'brandNames');
            }).end()
            .find('#browsers')
            .selectpicker()
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'browsers');
            }).end()
            .find('#preBranchVersion')
            .selectpicker()
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'preBranchVersion');
            }).end()
            .find('#preBuildTime')
            .selectpicker()
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'preBuildTime');
            }).end()
            .find('#siteURL')
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'URLfile');
            }).end()
            .find('#URLfile')
            .change(function (e) {
                $('#single-site-form').bootstrapValidator('revalidateField', 'siteURL');
            }).end()
            .bootstrapValidator({
                excluded: ':disabled',
                container: '.messages',
                feedbackIcons: {
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    buildType: {
                        validators: {
                            notEmpty: {
                                message: 'Build Type is required and cannot be empty'
                            }
                        }
                    },
                    siteURL: {
                        validators: {
                            uri: {
                                message: 'URL is not valid'
                            },
                            callback: {
                                message: 'Please choose only one from Site Address or Urls From File',
                                callback: function (value, validator, $field) {
                                    var foo = $('#single-site-form').find('#URLfile').val() == "";
                                    var bar = $('#single-site-form').find('#siteURL').val() == "";
                                    return ((foo && !bar) || (!foo && bar));
                                }
                            }
                        }
                    },
                    URLfile: {
                        validators: {
                            file: {
                                extension: 'txt',
                                message: 'Please select a text file'
                            },
                            callback: {
                                message: 'Please choose only one from Site Address or Urls From File',
                                callback: function (value, validator, $field) {
                                    var foo = $('#single-site-form').find('#URLfile').val() == "";
                                    var bar = $('#single-site-form').find('#siteURL').val() == "";
                                    return ((foo && !bar) || (!foo && bar));
                                }
                            }
                        }
                    },
                    brandName: {
                        validators: {
                            callback: {
                                message: 'Brand Name is required',
                                callback: function (value, validator, $field) {
                                    if (!$('#brandNameInput').hasClass('hidden'))
                                        return (value != "");
                                    return true;
                                }
                            }
                        }
                    },
                    brandNames: {
                        validators: {
                            callback: {
                                message: 'Brand Name is required, Please select',
                                callback: function (value, validator, $field) {
                                    if (!$('#brandNameSelect').hasClass('hidden'))
                                        return (value != "Choose Brand Name");
                                    return true;
                                }
                            }
                        }
                    },
                    username: {
                        validators: {
                            notEmpty: {
                                message: 'Username is required'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Password is required'
                            }
                        }
                    },
                    browsers: {
                        validators: {
                            notEmpty: {
                                message: 'Select at least one browser'
                            }
                        }
                    },
                    preBranchVersion: {
                        validators: {
                            callback: {
                                message: 'Select previous branch version for comparison',
                                callback: function (value, validator, $field) {
                                    if (!$('#brandNameSelect').hasClass("hidden")) {
                                        return (value != "Choose Pre Branch Version");
                                    }
                                    return true;
                                }
                            }
                        }
                    },
                    preBuildTime: {
                        validators: {
                            callback: {
                                message: 'Select previous build time for comparison',
                                callback: function (value, validator, $field) {
                                    if (!$('#brandNameSelect').hasClass("hidden"))
                                        return (value != "Choose Pre Build Time");
                                    return true;
                                }
                            }
                        }
                    },
                    fileURL: {
                        validators: {
                            file: {
                                extension: 'properties',
                                message: 'Please select a java properties file'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email(s) required'
                            }
                        }
                    }
                }
            })
            .on('success.form.bv', function (e) {
                e.preventDefault();
                var $form = $('#single-site-form');
                var bv = $form.data('bootstrapValidator');
                $('#mask').css('opacity', '0.9');
                $('#mask').css('display', 'block');
                $('#mask').width($('#sachin').width());
                $('#mask').removeClass('hidden');
                var msg = "";
                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    cache: false,
                    data: $form.serialize(),
                    success: function (result) {
                        var result = jQuery.parseJSON(result);
                        $("#single-site-form").replaceWith(formClone.clone());
                        $('#single-site-form').trigger('reset');
                        $('#mask').css('opacity', '0.9');
                        bv.disableSubmitButtons(false);
                        $('#mask').css('display', 'block');
                        $('#CrawlerConfigFile').val("");
                        $('#urlsTextFile').val("");
                        $("#mask #info #status").html("<b>Status: </b>" + result.status);
                        $("#mask #info #type").val(result.status);
                        $("#mask #info #buildID").val(result.buildID);
                        if (result.status == "Executing") {
                            msg = "You can wait until its complete or follow this <a target='_blank' href='" + result.url + "'>link</a> for detail information. ";
                            $("#mask #info #buildID").val(result.buildID);
                            $("#mask #info #buildUrl").val(result.url);
                        }
                        if (result.status == "inQueue") {
                            msg = "<br/><b>Reason: </b>" + result.reason + "Another site is running the test suite already. Your suite will run after suite is complete. You can wait until all validations are complete or follow this <a target='_blank' href='" + result.builds + "'>link</a> for detail information. ";
                            $("#mask #info #queueUrl").val(result.url);
                        }
                        $("#mask #info #msgs p").html("<b>Message: </b>");
                        $("#mask #info p").append(msg);
                        timer = setInterval(ajax_call, 4000);
                    }
                });
            });

// Polling
    var ajax_call = function () {
        var pollingData = {
            type: $('#mask #info #type').val(),
            buildID: $('#mask #info #buildID').val(),
            queueUrl: $('#mask #info #queueUrl').val(),
            buildUrl: $('#mask #info #buildUrl').val(),
            start: $('#mask #info #start').val()
        }
        $.ajax({
            url: 'Polling',
            type: 'POST',
            cache: false,
            data: pollingData,
            success: function (result) {
//                            var result = jQuery.parseJSON(result);
                var msg = "";
                $("#mask #info #status").html("<b>Status: </b>" + result.status);
                $("#mask #info #type").val(result.status);
                $("#mask #info #buildID").val(result.buildID);
                if (!result.polling) {
                    clearInterval(timer);
                }
                if (result.status == "Complete" || !result.polling) {
                    clearInterval(timer);
                    poll = false;
                    msg = "Test Suite is complete. View <a href='#' id='suite-report'>report</a>. ";
                    $("#mask #info #buildUrl").val(result.url);
                    $("#mask img").addClass("hidden");
                    $("#mask h3").html("Completed");
                    $("#resultsData >div#data").append(result.info);
                    $("#mask #bar-mask").addClass('hidden');
                    $('.result-image').addClass('hidden');
                }
                if (result.status == "inQueue") {
                    msg = "<br/><b>Reason: </b>" + result.reason + "Another site is running the test suite already. Your suite will run after suite is complete. You can wait until all validations are complete or follow this <a target='_blank' href='" + result.builds + "'>link</a> for detail information. ";
                    $("#mask #info #queueUrl").val(result.url);
                    $('.result-image').addClass('hidden');
                }
                if (result.status == "Executing") {
                    msg = "You can wait until its complete or follow this <a target='_blank' href='" + result.url + "'>link</a> for detail information. ";
                    $("#mask #info #buildID").val(result.buildID);
                    $("#mask #info #buildUrl").val(result.url);
                    $("#resultsData> div#data").append(result.info);
                    $('#info #start').val(result.length);
                    $('.result-image').removeClass('hidden');
//                                start = result.length;
                }
                $("#mask #info #msgs").html("<b>Message: </b>" + msg);
            }
        });
    };
    $('#buildType').change(function () {
        var val = $("#buildType option:selected").text();
        if (val == "Pre") {
            $('.preBranchVersion').addClass('hidden');
            $('.preBuildTime').addClass('hidden');
            $('#preBranchVersion').addClass('hidden');
            $('#preBuildTime').addClass('hidden');
            $('.diff').addClass('hidden');
            $('#brandNames').addClass('hidden');
            $('#brandNameSelect').addClass('hidden');
            $('#brandNameInput').removeClass('hidden');
        } else if (val == "Post") {
            $('.preBranchVersion').removeClass('hidden');
            $('.preBuildTime').removeClass('hidden');
            $('#preBranchVersion').removeClass('hidden');
            $('#preBuildTime').removeClass('hidden');
            $('.diff').removeClass('hidden');
            $('#brandNames').removeClass('hidden');
            $('#brandNameSelect').removeClass('hidden');
            $('#brandNameInput').addClass('hidden');
            $('#mask').css('opacity', '0.9');
            $('#mask').css('display', 'block');
            $('#mask').width($('#sachin').width());
            $('#mask').removeClass('hidden');
            $.ajax({
                url: "BrandNames",
                type: 'POST',
                cache: false,
                success: function (result) {
                    var obj = jQuery.parseJSON(result);
                    $('#mask').addClass('hidden');
                    $('#mask').css('display', 'none');
                    $('select#brandNames').html("");
                    $('select#brandNames').append(' <option selected>Choose Brand Name</option>');
                    $.each(obj, function (key, value) {
                        $('select#brandNames').append('<option>' + value + '</option>');
                    });
                    $('select#brandNames').selectpicker('refresh');
                }
            });
        }
    });
    $('select#brandNames').change(function () {
        var value = $('select#brandNames').find(':selected').text();
        if (value != "Choose Brand Name") {
            $('#mask').css('opacity', '0.9');
            $('#mask').css('display', 'block');
            $('#mask').width($('#sachin').width());
            $('#mask').removeClass('hidden');
            $.ajax({
                url: "BranchVersions?brand=" + value,
                type: 'GET',
                cache: false,
                success: function (result) {
                    var obj = jQuery.parseJSON(result);
                    $('#mask').addClass('hidden');
                    $('#mask').css('display', 'none');
                    $('#preBranchVersion').html('');
                    $('#preBranchVersion').append('<option selected>Choose Pre Branch Version</option>');
                    $.each(obj, function (key, value) {
                        $('#preBranchVersion').append('<option>' + value + '</option>');
                    });
                    $('#preBranchVersion').selectpicker('refresh');
                }
            });
        } else {
            $('#preBranchVersion').html('');
            $('#preBranchVersion').append('<option selected>Choose Pre Branch Version</option>');
            $('#preBranchVersion').selectpicker('refresh');
            $('#preBuildTime').html('');
            $('#preBuildTime').append(' <option selected>Choose Pre Build Time</option>');
            $('#preBuildTime').selectpicker('refresh');
            $('#single-site-form').bootstrapValidator('revalidateField', 'preBranchVersion');
            $('#single-site-form').bootstrapValidator('revalidateField', 'preBuildTime');
        }
    });
    $('#preBranchVersion').change(function () {
        var brand = $('select#brandNames').find(':selected').text();
        var branch = $('#preBranchVersion').find(':selected').text();
        if (brand != "Choose Brand Name") {
            if (branch != "Choose Pre Branch Version") {
                $('#mask').css('opacity', '0.9');
                $('#mask').css('display', 'block');
                $('#mask').width($('#sachin').width());
                $('#mask').removeClass('hidden');
                $.ajax({
                    url: "BuildTime?brand=" + brand + "&branch=" + branch,
                    type: 'GET',
                    cache: false,
                    success: function (result) {
                        var obj = jQuery.parseJSON(result);
                        $('#mask').addClass('hidden');
                        $('#mask').css('display', 'none');
                        $('#preBuildTime').html('');
                        $('#preBuildTime').append(' <option selected>Choose Pre Build Time</option>');
                        $.each(obj, function (key, value) {
                            $('#preBuildTime').append('<option>' + value + '</option>');
                        });
                        $('#preBuildTime').selectpicker('refresh');
                    }
                });
            } else {
                $('#preBuildTime').html('');
                $('#preBuildTime').append(' <option selected>Choose Pre Build Time</option>');
                $('#preBuildTime').selectpicker('refresh');
                $('#single-site-form').bootstrapValidator('revalidateField', 'preBuildTime');
            }
        }
    });
    $('#setAuthentication').click(function () {
        if ($('#setAuthentication').is(':checked')) {
            $('#username').removeAttr('disabled');
            $('#password').removeAttr('disabled');
        } else {
            $('#username').attr('disabled', 'disabled');
            $('#password').attr('disabled', 'disabled');
        }
    });
    $('#fileURL').fileupload({
        dataType: 'json',
        url: 'FileUploadServlet',
        type: 'POST',
        singleFileUploads: true,
        autoUpload: true,
        replaceFileInput: false,
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.crawler #bar #orangeBar').css(
                    'width',
                    progress + '%'
                    );
        },
        add: function (e, data) {
            $('.crawler #bar #orangeBar').addClass('progress-bar-info');
            $('.crawler #bar #orangeBar').addClass('bg-info');
            $('.crawler #bar #orangeBar').removeClass('bg-danger');
            $('.crawler #bar #orangeBar').removeClass('progress-bar-danger');
            $('.crawler #bar').removeClass('hidden');
            $('.crawler #bar #orangeBar').text('Uploading File...');
            data.submit();
        },
        done: function (e, data) {
            $('.crawler #bar #orangeBar').text('Upload Finished...');
            $('#CrawlerConfigFile').val(data.result[0].path);
        },
        fail: function (e, data) {
            $('.crawler #bar #orangeBar').removeClass('progress-bar-info');
            $('.crawler #bar #orangeBar').removeClass('bg-info');
            $('.crawler #bar #orangeBar').addClass('bg-danger');
            $('.crawler #bar #orangeBar').addClass('progress-bar-danger');
            $('.crawler #bar #orangeBar').text(data.errorThrown);
        },
        change: function (e, data) {
            $('#CrawlerConfigFile').val("");
            $('.crawler #bar').addClass('hidden');
            $('.crawler #bar #orangeBar').addClass('progress-bar-info');
            $('.crawler #bar #orangeBar').addClass('bg-info');
            $('.crawler #bar #orangeBar').removeClass('bg-danger');
            $('.crawler #bar #orangeBar').removeClass('progress-bar-danger');
            var name = $('#fileURL').val();
            if (name == "") {
                $('.crawler #bar').addClass('hidden');
            }
        }
    });
    $('#URLfile').fileupload({
        dataType: 'json',
        url: 'UrlUploadServlet',
        type: 'POST',
        singleFileUploads: true,
        autoUpload: true,
        replaceFileInput: false,
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.URLfile #bar #orangeBar').css(
                    'width',
                    progress + '%'
                    );
        },
        add: function (e, data) {
            $('.URLfile #bar #orangeBar').addClass('progress-bar-info');
            $('.URLfile #bar #orangeBar').addClass('bg-info');
            $('.URLfile #bar #orangeBar').removeClass('bg-danger');
            $('.URLfile #bar #orangeBar').removeClass('progress-bar-danger');
            $('.URLfile #bar').removeClass('hidden');
            $('.URLfile #bar #orangeBar').text('Uploading File...');
            data.submit();
        },
        done: function (e, data) {
            $('.URLfile #bar #orangeBar').text('Upload Finished...');
            $('#urlsTextFile').val(data.result[0].path);
        },
        fail: function (e, data) {
            $('.URLfile #bar #orangeBar').removeClass('progress-bar-info');
            $('.URLfile #bar #orangeBar').removeClass('bg-info');
            $('.URLfile #bar #orangeBar').addClass('bg-danger');
            $('.URLfile #bar #orangeBar').addClass('progress-bar-danger');
            $('.URLfile #bar #orangeBar').text(data.errorThrown);
        },
        change: function (e, data) {
            $('#urlsTextFile').val("");
            $('.URLfile #bar').addClass('hidden');
            $('.URLfile #bar #orangeBar').addClass('progress-bar-info');
            $('.URLfile #bar #orangeBar').addClass('bg-info');
            $('.URLfile #bar #orangeBar').removeClass('bg-danger');
            $('.URLfile #bar #orangeBar').removeClass('progress-bar-danger');
            var name = $('#URLfile').val();
            if (name == "") {
                $('.URLfile #bar').addClass('hidden');
            }
        }
    });
    $('#suite-report').click(function () {
        $('html,body').animate({
            scrollTop: $(".author").offset().top},
        'slow');
    });
});
